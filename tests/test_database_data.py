import pytest
from peewee import SqliteDatabase

from people_stats.data_load import DataHandler
from people_stats.models import MODELS, Person


@pytest.fixture(scope="session", autouse=True)
def test_database_setup():
    """does this runs only once per all tests?
    if not use pytest_sessionstart"""
    test_db = SqliteDatabase(":memory:")
    test_db.bind(MODELS)
    test_db.create_tables(MODELS)

    dt = DataHandler(is_test=True)
    data = dt.load_from_file("data/test_persons.json")
    for person in data:
        values = dt.parse_json(person)
        dt.save_to_database(values)

    return test_db


@pytest.fixture
def data_handler():
    return DataHandler(is_test=True)


def test_people_insert():
    assert Person.select().count() == 5


def test_gender_percentage(data_handler):
    women, men = data_handler.gender_percentage()
    assert women == 60
    assert men == 40


def test_average_age_all(data_handler):
    age = data_handler.average_age("all")
    assert age == pytest.approx(24, 0.1)


def test_average_age_women(data_handler):
    age = data_handler.average_age("women")
    assert age == pytest.approx(20, 0.1)


def test_average_age_men(data_handler):
    age = data_handler.average_age("men")
    assert age == pytest.approx(30, 0.1)


def test_most_common_cities(data_handler):
    cities = data_handler.most_common_cities(2)
    assert len(cities) == 2
    assert cities[0] == ("Test", 3)
    assert cities[1] == ("Test2", 2)


def test_most_common_passwords(data_handler):
    passwords = data_handler.most_common_passwords(2)
    assert len(passwords) == 2
    assert passwords[0] == ("admin1", 3)
    assert passwords[1] == ("hunter2", 2)


def test_born_in_time_period(data_handler):
    people = data_handler.born_in_time_period("1999-01-01", "2000-08-01")
    assert len(people) == 2
    assert people[0] == (
        "foo",
        "Fake",
        "Name",
        "no-spam-pls@example.com",
        "2000-04-02T21:37:00.000Z",
    )
    assert people[1] == (
        "bar",
        "Name",
        "Fake",
        "invalid@mail.localhost",
        "2000-07-05T13:37:00.000Z",
    )


def test_born_in_time_period_bad_no_people(data_handler):
    people = data_handler.born_in_time_period("1999-01-01", "2000-04-01")
    assert people == []


def test_born_in_time_period_bad(data_handler):
    people = data_handler.born_in_time_period("foo", "bar")
    assert people == []


def test_most_secure_passwords(data_handler):
    passwords = data_handler.most_secure_passwords(2)
    assert len(passwords) == 2
    assert passwords[0] == (2, "admin1")
    assert passwords[1] == (2, "hunter2")


def test_score_passwords(data_handler):
    assert data_handler.score_password("owg2jUI(O$R%J$GHJOI") == 12
    assert data_handler.score_password("supertajne") == 6
    assert data_handler.score_password("Ab1337") == 4
