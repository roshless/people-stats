import pytest


@pytest.fixture
def cli_parser():
    from people_stats.cli import PeopleStatsCLI

    cli = PeopleStatsCLI()
    return cli.create_parser()


# gender_percentage


def test_cli_gender_percentage(cli_parser):
    out = cli_parser.parse_args(["--gender-percentage"])
    assert out.gender_percentage is True


# average_age


def test_cli_average_age_all_short(cli_parser):
    out = cli_parser.parse_args(["--average-age"])
    assert out.average_age == "all"


def test_cli_average_age_all_full(cli_parser):
    out = cli_parser.parse_args(["--average-age", "all"])
    assert out.average_age == "all"


def test_cli_average_age_women(cli_parser):
    out = cli_parser.parse_args(["--average-age", "women"])
    assert out.average_age == "women"


def test_cli_average_age_men(cli_parser):
    out = cli_parser.parse_args(["--average-age", "men"])
    assert out.average_age == "men"


def test_cli_average_age_bad(cli_parser):
    with pytest.raises(SystemExit) as e:
        cli_parser.parse_args(["--average-age", "foobar"])
    assert e.value.code != 0


# most_common_cities


def test_cli_most_common_cities(cli_parser):
    out = cli_parser.parse_args(["--most-common-cities", "15"])
    assert out.most_common_cities == 15


def test_cli_most_common_cities_no_n(cli_parser):
    with pytest.raises(SystemExit) as e:
        cli_parser.parse_args(["--most-common-cities"])
    assert e.value.code != 0


def test_cli_most_common_cities_bad(cli_parser):
    with pytest.raises(SystemExit) as e:
        cli_parser.parse_args(["--most-common-cities", "foobar"])
    assert e.value.code != 0


# most_common_passwords


def test_cli_most_common_passwords(cli_parser):
    out = cli_parser.parse_args(["--most-common-passwords", "15"])
    assert out.most_common_passwords == 15


def test_cli_most_common_passwords_no_n(cli_parser):
    with pytest.raises(SystemExit) as e:
        cli_parser.parse_args(["--most-common-passwords"])
    assert e.value.code != 0


def test_cli_most_common_passwords_bad(cli_parser):
    with pytest.raises(SystemExit) as e:
        cli_parser.parse_args(["--most-common-passwords", "foobar"])
    assert e.value.code != 0


# born_in_time_period


def test_cli_born_in_time_period(cli_parser):
    out = cli_parser.parse_args(["--born-in-time-period", "2010-01-01", "2020-01-01"])
    assert out.born_in_time_period == ["2010-01-01", "2020-01-01"]


def test_cli_born_in_time_period_one_argument(cli_parser):
    with pytest.raises(SystemExit) as e:
        cli_parser.parse_args(["--born-in-time-period", "2010-01-01"])
    assert e.value.code != 0


def test_cli_born_in_time_period_three_argument(cli_parser):
    with pytest.raises(SystemExit) as e:
        cli_parser.parse_args(
            ["--born-in-time-period", "2010-01-01", "2015-01-01", "2020-01-01"]
        )
    assert e.value.code != 0


def test_cli_born_in_time_period_no_arguments(cli_parser):
    with pytest.raises(SystemExit) as e:
        cli_parser.parse_args(["--born-in-time-period"])
    assert e.value.code != 0


# most_secure_passwords


def test_cli_most_secure_passwords(cli_parser):
    out = cli_parser.parse_args(["--most-secure-passwords", "15"])
    assert out.most_secure_passwords == 15


def test_cli_most_secure_passwords_no_n(cli_parser):
    with pytest.raises(SystemExit) as e:
        cli_parser.parse_args(["--most-secure-passwords"])
    assert e.value.code != 0


def test_cli_most_secure_passwords_bad(cli_parser):
    with pytest.raises(SystemExit) as e:
        cli_parser.parse_args(["--most-secure-passwords", "foobar"])
    assert e.value.code != 0
